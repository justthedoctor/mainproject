var path = require('path');
var request = require('request');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: true });
var express = require('express');
var app = express();

  var httpPort = 5150;
  var httpHost = "localhost";

        // App Variables
      appName = "mainProject";
      appVersion = "0.0.0a";
      appRepo = "https://gitlab.com/justthedoctor/mainproject";

    app.set("view engine", "pug");
    app.set("views", path.join(__dirname, "views"));
    app.use('/public', express.static('public'));

    var server = app.listen(httpPort, httpHost, function () {
      console.log(appName + " Version: "+ appVersion);
      console.log('HTTP running on http://' + httpHost + ':' + httpPort);
    });

    app.get("/", (req, res) => {
        reqHeaders = JSON.stringify(req.headers);
        res.render("default");
      });
